package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testShouldBeDefineAPlanetAsAGrid() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
	}
	
	@Test
	public void testShouldBePlanetContainsObstacle() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		assertTrue(obstacle);
	}
	
	@Test
	public void testShouldBeNotPlanetContainsObstacle() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		boolean obstacle = rover.planetContainsObstacleAt(2, 4);
		assertFalse(obstacle);
	}
	
	@Test
	public void testShouldBeAnEmptyCommandString() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setRoverX(0);
		rover.setRoverY(0);
		rover.setFacing("N");
		String commandString = "";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,N)",returnString);
	}
	
	@Test
	public void testRoverShouldTurnRight() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setRoverX(0);
		rover.setRoverY(0);
		rover.setFacing("N");
		String commandString = "r";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,E)",returnString);
	}
	
	@Test
	public void testRoverShouldTurnLeft() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setRoverX(0);
		rover.setRoverY(0);
		rover.setFacing("N");
		String commandString = "l";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(0,0,W)",returnString);
	}
	
	@Test
	public void testRoverShouldGoForward() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setRoverX(7);
		rover.setRoverY(6);
		rover.setFacing("N");
		String commandString = "f";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(7,7,N)",returnString);
	}
	
	@Test
	public void testRoverShouldGoBackWord() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setRoverX(5);
		rover.setRoverY(8);
		rover.setFacing("E");
		String commandString = "b";
		String returnString = rover.executeCommand(commandString);
		assertEquals("(4,8,E)",returnString);
	}
	
	@Test
	public void testRoverShouldBeMovingCombined() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setRoverX(0);
		rover.setRoverY(0);
		rover.setFacing("N");
		String commandString = "f";
		String commandString2 = "r";
		String returnString = rover.executeCommand(commandString);
		returnString = rover.executeCommand(commandString);
		returnString = rover.executeCommand(commandString2);
		returnString = rover.executeCommand(commandString);
		returnString = rover.executeCommand(commandString);
		assertEquals("(2,2,E)",returnString);
	}
	
	
}
